# Code Interview Instructions

We would like you to create a new leads portal view for AAG, the comps for the view are attached. The data for this portal view should be dynamic and come from the provided end point. The view should be responsive down to tablet size and match the tablet comp provided. The user should be able to sort each leads by at minimum the id, date, and quality. Please use whatever sorting library you are comfortable with, or roll your own solution. No front end framworks are allowed as our current code base does not use front end frame works, this include (react, angular, etc). Jquery is fine to use for this project. You may also use a front end css library like bootstrap if you feel the need. Last but not least, please write REUSABLE production ready quality code for this project. The technologies needed for this project are HTML, CSS, and JavaScript only.

## Important URLs

1. Data Endpoint: `https://api.myjson.com/bins/unxs8`
2. jquery CDN: `https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js`

## Important Colors

1. header: `#0A476D`
2. main background: `#ecf0f1`
3. summary Numbers: `#027499`
4. more button: `#d0151c`

## Final Instructions
1. Reference the comps from the repository: `/comps/screencapture-1.pdf` and `/comps/screencapture-2.png`
2. Follow the `Code Interview Instructions` above to create your application.
3. Create a pull request for your application so we can review your code 

## Time Allowed to Complete This Test: 60 MINUTES



